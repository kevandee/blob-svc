package data

import (
	"gitlab.com/distributed_lab/kit/pgdb"
)

type BlobQ interface {
	New() BlobQ

	Get() (*Blob, error)
	Select() ([]Blob, error)

	Transaction(fn func(q BlobQ) error) error

	Insert(data Blob) (Blob, error)

	Page(pageParams pgdb.OffsetPageParams) BlobQ

	FilterByID(id ...int64) BlobQ
	FilterByOwner(owners ...string) BlobQ
	DeleteById(id ...int64) (*Blob, error)
}

type Blob struct {
	ID    int64  `db:"id" structs:"-"`
	Owner string `db:"owner" structs:"-"`
	Data  Data   `db:"data" structs:"-"`
}
