package data

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
)

const (
	BlobDataTemplate = "blob-data-template"
)

type Data json.RawMessage

func (m Data) Value() (driver.Value, error) {
	j, err := json.Marshal(m)
	return j, err
}

func (m *Data) Scan(src interface{}) error {
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion .([]byte) failed.")
	}

	return json.Unmarshal(source, m)
}

type TemplateDataAttributes struct {
	Payload *json.RawMessage `json:"payload"`
	Locale  *string          `json:"locale"`
	Files   []string         `json:"files"`
}
