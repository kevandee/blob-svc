package pg

import (
	"blob-svc/internal/data"
	"database/sql"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"github.com/fatih/structs"
	"gitlab.com/distributed_lab/kit/pgdb"
)

const blobsTableName = "blobs"

func NewBlobsQ(db *pgdb.DB) data.BlobQ {
	return &blobsQ{
		db:  db.Clone(),
		sql: sq.Select("n.*").From(fmt.Sprintf("%s as n", blobsTableName)),
	}
}

type blobsQ struct {
	db  *pgdb.DB
	sql sq.SelectBuilder
}

func (q *blobsQ) New() data.BlobQ {
	return NewBlobsQ(q.db)
}

func (q *blobsQ) Get() (*data.Blob, error) {
	var result data.Blob
	err := q.db.Get(&result, q.sql)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	return &result, err
}

func (q *blobsQ) Select() ([]data.Blob, error) {
	var result []data.Blob
	err := q.db.Select(&result, q.sql)
	return result, err
}

func (q *blobsQ) Transaction(fn func(q data.BlobQ) error) error {
	return q.db.Transaction(func() error {
		return fn(q)
	})
}

func (q *blobsQ) Insert(value data.Blob) (data.Blob, error) {
	clauses := structs.Map(value)
	clauses["data"] = value.Data
	clauses["owner"] = value.Owner
	var result data.Blob
	stmt := sq.Insert(blobsTableName).SetMap(clauses).Suffix("returning *")
	err := q.db.Get(&result, stmt)

	return result, err
}

func (q *blobsQ) Page(pageParams pgdb.OffsetPageParams) data.BlobQ {
	q.sql = pageParams.ApplyTo(q.sql, "id")
	return q
}

func (q *blobsQ) FilterByID(ids ...int64) data.BlobQ {
	q.sql = q.sql.Where(sq.Eq{"n.id": ids})
	return q
}

func (q *blobsQ) FilterByOwner(owners ...string) data.BlobQ {
	q.sql = q.sql.Where(sq.Eq{"n.owner": owners})
	return q
}

func (q *blobsQ) DeleteById(ids ...int64) (*data.Blob, error) {
	blob, err := q.FilterByID(ids...).Get()
	if err != nil {
		return nil, err
	}
	sqlStatement := `DELETE FROM ` + blobsTableName + ` WHERE id = $1;`

	err = q.db.ExecRaw(sqlStatement, ids[0])
	return blob, err
}
