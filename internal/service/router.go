package service

import (
	"blob-svc/internal/data/pg"
	"blob-svc/internal/service/handlers"
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
)

func (s *service) router() chi.Router {
	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxBlobQ(pg.NewBlobsQ(s.cfg.DB())),
			handlers.CtxHorizonCfg(s.cfg.Horizon()),
		),
	)

	r.Route("/blobs", func(r chi.Router) {
		r.Get("/", handlers.GetBlobsList)
		r.Post("/", handlers.AddBlob)
		r.Route("/{id}", func(r chi.Router) {
			r.Get("/", handlers.GetBlob)
			r.Delete("/", handlers.RemoveBlob)
		})
	})

	r.Post("/accounts", handlers.CreateAccount)

	return r
}
