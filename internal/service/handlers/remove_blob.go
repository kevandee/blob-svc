package handlers

import (
	"blob-svc/resources"
	"net/http"

	"blob-svc/internal/service/requests"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func RemoveBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewRemoveBlobRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	blob, err := BlobQ(r).DeleteById(request.BlobID)
	if blob == nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}
	if err != nil {
		Log(r).WithError(err).Error("failed to get notification")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	response := resources.BlobResponse{
		Data: newBlobModel(*blob),
	}
	ape.Render(w, response)
}
