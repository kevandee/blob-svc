package handlers

import (
	"blob-svc/internal/data"
	"blob-svc/internal/service/requests"
	"blob-svc/resources"
	"bytes"
	"encoding/json"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	depkeypair "gitlab.com/tokend/go/keypair"
	"gitlab.com/tokend/go/signcontrol"
	"gitlab.com/tokend/go/xdr"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/keypair"
	"io/ioutil"
	"net/http"
	"strconv"
)

type CoreTransactionRequest struct {
	Transaction string `json:"tx"`
}

func CreateAccount(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewCreateAccountRequest(r)
	if err != nil {
		Log(r).WithError(err).Info("wrong request")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	requestData := request.Data.Attributes

	horizonCfg := HorizonCfg(r)
	txBuilder := xdrbuild.NewBuilder(horizonCfg["network_passphrase"].(string), int64(horizonCfg["tx_expiration_time"].(int)))
	tx := txBuilder.Transaction(keypair.MustParseAddress(requestData.SourceAccount))
	tx = tx.Op(&xdrbuild.CreateAccount{
		Destination: requestData.Operation.Body.CreateAccountOp.Destination,
		RoleID:      requestData.Operation.Body.CreateAccountOp.RoleID,
		Signers:     getSigners(requestData.Operation.Body.CreateAccountOp.SignersData),
	})

	envelopeStr, err := tx.Marshal()
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(errors.Wrap(err, "failed to build tx envelope"))...)
		return
	}

	var envelope xdr.TransactionEnvelope
	err = envelope.Scan(envelopeStr)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(errors.New("failed to extract envelope info"))...)
		return
	}

	signedEnvelope, err := txBuilder.Sign(&envelope, keypair.MustParseSeed(horizonCfg["seed"].(string)))
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(errors.Wrap(err, "failed to sign tx"))...)
		return
	}

	signedEnvelopeStr, err := xdr.MarshalBase64(signedEnvelope)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(errors.Wrap(err, "failed to marshal tx envelope"))...)
		return
	}

	env := CoreTransactionRequest{Transaction: signedEnvelopeStr}
	reqBB, err := json.Marshal(env)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(errors.Wrap(err, "failed to marshal core envelope"))...)
		return
	}

	endpoint := horizonCfg["endpoint"].(string)
	port := strconv.Itoa(horizonCfg["addr"].(int))
	txUrl := horizonCfg["tx_endpoint"].(string)
	endpoint = endpoint + ":" + port + txUrl

	respBB, err := PostJsonWithSeed(r, endpoint, reqBB, horizonCfg["seed"].(string))
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(errors.Wrap(err, "failed to post tx"))...)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.Write(respBB)
}

func getSigners(accountSigners []resources.SignerData) []xdrbuild.SignerData {
	var signers []xdrbuild.SignerData
	for _, accountSigner := range accountSigners {
		signers = append(signers, xdrbuild.SignerData{
			PublicKey: accountSigner.PublicKey,
			RoleID:    accountSigner.RoleID,
			Weight:    accountSigner.Weight,
			Identity:  accountSigner.Identity,
			Details:   data.Details{},
		})
	}
	return signers
}

func PostJsonWithSeed(r *http.Request, endpoint string, reqBB []byte, seed string) ([]byte, error) {
	request, err := http.NewRequest("POST", endpoint, bytes.NewReader(reqBB))
	if err != nil {
		return nil, errors.Wrap(err, "Failed to create POST http.Request")
	}
	request = request.WithContext(r.Context())

	request.Header.Set("content-type", "application/json")

	err = signcontrol.SignRequest(request, depkeypair.MustParse(seed))
	if err != nil {
		return nil, errors.Wrap(err, "Failed to sign request")
	}

	client := http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to perform http request")
	}

	defer request.Body.Close()

	respBB, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to read response body", logan.F{
			"status_code": response.StatusCode,
		})
	}

	return respBB, nil
}
