package handlers

import (
	"blob-svc/resources"
	"encoding/json"
	"net/http"

	"blob-svc/internal/data"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"

	"blob-svc/internal/service/requests"
)

func AddBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewAddBlobRequest(r)
	if err != nil {
		Log(r).WithError(err).Info("wrong request")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	var resultBlob data.Blob

	blob := data.Blob{
		Owner: request.Data.Relationships.Owner,
		Data:  data.Data(request.Data.Attributes.Content),
	}
	resultBlob, err = BlobQ(r).Insert(blob)
	if err != nil {
		Log(r).WithError(err).Error("failed to insert blob")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	result := resources.BlobResponse{
		Data: newBlobModel(resultBlob),
	}
	ape.Render(w, result)
}

func newBlobModel(blob data.Blob) resources.Blob {
	result := resources.Blob{
		Key:           resources.NewKeyInt64(blob.ID, resources.BLOB),
		Relationships: resources.BlobRelationships{Owner: blob.Owner},
		Attributes:    resources.BlobAttributes{Content: json.RawMessage(blob.Data)},
	}

	return result
}
