package handlers

import (
	"net/http"

	"blob-svc/resources"

	"blob-svc/internal/data"
	"blob-svc/internal/service/requests"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func GetBlobsList(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetBlobsListRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	blobQ := BlobQ(r)
	applyFilters(blobQ, request)
	blobs, err := blobQ.Select()
	if err != nil {
		Log(r).WithError(err).Error("failed to get blobs")
		ape.Render(w, problems.InternalError())
		return
	}

	blobsIds := make([]int64, len(blobs))
	for i, item := range blobs {
		blobsIds[i] = item.ID
	}

	response := resources.BlobListResponse{
		Data:  newBlobsList(blobs),
		Links: GetOffsetLinks(r, request.OffsetPageParams),
	}

	ape.Render(w, response)
}

func newBlobsList(blobs []data.Blob) []resources.Blob {
	result := make([]resources.Blob, len(blobs))
	for i, blob := range blobs {
		result[i] = newBlobModel(blob)
	}
	return result
}

func applyFilters(q data.BlobQ, request requests.GetBlobsListRequest) {
	q.Page(request.OffsetPageParams)

	if len(request.FilterOwner) > 0 {
		q.FilterByOwner(request.FilterOwner...)
	}
}
