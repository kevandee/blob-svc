package requests

import (
	"blob-svc/resources"
	"encoding/json"
	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"net/http"
)

type CreateAccountRequest struct {
	Data resources.TxCreateAccount
}

func NewCreateAccountRequest(r *http.Request) (CreateAccountRequest, error) {
	var request CreateAccountRequest
	if err := json.NewDecoder(r.Body).Decode(&request.Data); err != nil {
		return request, errors.Wrap(err, "failed to unmarshal")
	}

	return request, request.validate()
}

func (r *CreateAccountRequest) validate() error {
	return mergeErrors(validation.Errors{
		"attributes": validation.Validate(&r.Data.Attributes, validation.Required),
	},
	).Filter()
}
