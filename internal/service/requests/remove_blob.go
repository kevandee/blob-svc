package requests

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/spf13/cast"
)

type RemoveBlobRequest struct {
	BlobID int64 `url:"-"`
}

func NewRemoveBlobRequest(r *http.Request) (RemoveBlobRequest, error) {
	request := RemoveBlobRequest{}

	request.BlobID = cast.ToInt64(chi.URLParam(r, "id"))

	return request, nil
}
