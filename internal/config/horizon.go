package config

import "gitlab.com/distributed_lab/kit/kv"

func (c *ViperConfig) Horizon() map[string]interface{} {
	return kv.MustGetStringMap(c.getter, "horizon")
}
