openapi: 3.0.0
info:
  version: 1.0.0
  title: blob-svc
  description: ''
servers:
  - url: 'https://api.demo.tokend.io'
    description: TokenD Developer Environment
paths:
  /accounts:
    post:
      summary: Create account transaction
      operationId: createAccount
      requestBody:
        content:
          application/json:
            schema:
              type: object
              required:
                - tx
              properties:
                tx:
                  $ref: '#/components/schemas/TxCreateAccount'
      response:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: object
                    format: json.RawMessage
  /blobs:
    post:
      tags:
        - Blob
      summary: Add new user blob
      opperationId: addUserBlob
      requestBody:
        content:
          application/json:
            schema:
              type: object
              required:
                - data
                - owner
              properties:
                owner:
                  type: string
                data:
                  type: object
      response:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: object
                    $ref: '#/components/schemas/Blob'
    get:
      summary: Get blobs list
      operationId: getBlobsList
      parameters:
        - $ref: '#/components/parameters/pageNumberParam'
        - $ref: '#/components/parameters/pageLimitParam'
        - $ref: '#/components/parameters/sortingParam'
        - in: query
          name: 'filter[owner]'
          required: false
          schema:
            type: string
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: array
                    items:
                      $ref: '#/components/schemas/Blob'
                  links:
                    type: object
                    description: Provides links for pagination
                    properties:
                      next:
                        type: string
                        description: link to the next page
                      self:
                        type: string
                        description: link to the current page
  '/blobs/{id}':
    parameters:
      - name: id
        in: path
        required: true
        schema:
          type: int
    get:
      tags:
        - Blob
      summary: Get blob by id
      operationId: getBlob
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: object
                    $ref: '#/components/schemas/Blob'
        '404':
          description: Not found
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: object
                    $ref: '#/components/schemas/NotFoundError'
    delete:
      tags:
        - Blob
      summary: Remove blob by id
      operationId: removeBlob
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: object
                    $ref: '#/components/schemas/Blob'
        '404':
          description: Not found
          content:
            application/json:
              schema:
                type: object
                properties:
                  data:
                    type: object
                    $ref: '#/components/schemas/NotFoundError'
components:
  schemas:
    Blob:
      allOf:
        - $ref: '#/components/schemas/BlobKey'
        - type: object
          required:
            - relationships
            - attributes
          properties:
            relationships:
              type: object
              required:
                - owner
              properties:
                owner:
                  type: string
            attributes:
              type: object
              required:
                - content
              properties:
                content:
                  type: object
                  format: json.RawMessage
    BlobKey:
      type: object
      required:
        - id
        - type
      properties:
        id:
          type: string
        type:
          type: string
          enum:
            - blob
    CreateAccountOp:
      type: object
      tags:
        - create_account
      required:
        - destination
        - referrer
        - roleID
        - signersData
      properties:
        destination:
          type: string
        referrer:
          type: string
        roleID:
          type: uint64
        signersData:
          type: array
          items:
            $ref: '#/components/schemas/SignerData'
    NotFoundError:
      type: object
      required:
        - code
      properties:
        code:
          type: int32
        message:
          type: string
    Operation:
      type: object
      required:
        - body
      properties:
        body:
          $ref: '#/components/schemas/OptionBody'
    OptionBody:
      type: object
      required:
        - type
        - createAccountOp
      properties:
        type:
          type: string
        createAccountOp:
          $ref: '#/components/schemas/CreateAccountOp'
    SignerData:
      type: object
      tags:
        - create_account
      required:
        - publicKey
        - roleID
        - weight
        - identity
        - details
      properties:
        publicKey:
          type: string
        roleID:
          type: uint64
        weight:
          type: uint32
        identity:
          type: uint32
        details:
          type: string
    TxCreateAccount:
      allOf:
        - $ref: '#/components/schemas/TxCreatAccountKey'
        - type: object
          tags:
            - create_account
          required:
            - attributes
          properties:
            attributes:
              type: object
              required:
                - sourceAccount
                - operation
              properties:
                sourceAccount:
                  type: string
                operation:
                  $ref: '#/components/schemas/Operation'
    TxCreateAccountKey:
      type: object
      required:
        - id
        - type
      properties:
        id:
          type: string
        type:
          type: string
          enum:
            - txCreateAccount
  parameters:
    pageLimitParam:
      in: query
      name: 'page[limit]'
      required: false
      schema:
        type: integer
        minimum: 1
        maximum: 100
        default: 15
        description: Numbers of items per page to return.
    pageNumberParam:
      in: query
      name: 'page[number]'
      required: false
      schema:
        type: integer
      description: The number of a page to return.
    sortingParam:
      in: query
      name: 'page[order]'
      required: false
      schema:
        type: string
        enum:
          - asc
          - desc
        default: desc
        description: 'Order of records on the page. If sortingParam is not specified, order of records is by default sorted by ID.'
