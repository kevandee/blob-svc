/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type NotFoundError struct {
	Code    int32   `json:"code"`
	Message *string `json:"message,omitempty"`
}
