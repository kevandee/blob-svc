/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type TxCreateAccountAttributes struct {
	Operation     Operation `json:"operation"`
	SourceAccount string    `json:"sourceAccount"`
}
