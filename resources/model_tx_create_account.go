/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type TxCreateAccount struct {
	Key
	Attributes TxCreateAccountAttributes `json:"attributes"`
}
type TxCreateAccountResponse struct {
	Data     TxCreateAccount `json:"data"`
	Included Included        `json:"included"`
}

type TxCreateAccountListResponse struct {
	Data     []TxCreateAccount `json:"data"`
	Included Included          `json:"included"`
	Links    *Links            `json:"links"`
}

// MustTxCreateAccount - returns TxCreateAccount from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustTxCreateAccount(key Key) *TxCreateAccount {
	var txCreateAccount TxCreateAccount
	if c.tryFindEntry(key, &txCreateAccount) {
		return &txCreateAccount
	}
	return nil
}
